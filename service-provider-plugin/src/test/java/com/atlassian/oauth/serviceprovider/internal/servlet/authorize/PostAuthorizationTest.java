package com.atlassian.oauth.serviceprovider.internal.servlet.authorize;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.Consumer.SignatureMethod;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.oauth.serviceprovider.internal.ByteArrayServletOutputStream;
import com.atlassian.oauth.serviceprovider.internal.Randomizer;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.URI;

import static com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version.V_1_0;
import static com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version.V_1_0_A;
import static com.atlassian.oauth.testing.Matchers.equalTo;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static com.atlassian.oauth.testing.TestData.KEYS;
import static com.atlassian.oauth.testing.TestData.USER;
import static net.oauth.OAuth.OAUTH_CALLBACK;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PostAuthorizationTest {
    public static final Consumer CONSUMER_WITHOUT_CALLBACK = Consumer.key("consumer-rsa").name("Consumer using RSA").description("description").signatureMethod(SignatureMethod.RSA_SHA1).publicKey(KEYS.getPublic()).build();
    private static final ServiceProviderToken UNAUTHORIZED_REQUEST_TOKEN = ServiceProviderToken.newRequestToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).version(V_1_0_A).build();
    private static final ServiceProviderToken UNAUTHORIZED_REQUEST_TOKEN_WITH_CUSTOM_CALLBACK = ServiceProviderToken.newRequestToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).callback(URI.create("http://consumer/other-callback")).version(V_1_0_A).build();
    private static final ServiceProviderToken UNAUTHORIZED_REQUEST_TOKEN_WITHOUT_CALLBACK = ServiceProviderToken.newRequestToken("1234").tokenSecret("5678").consumer(CONSUMER_WITHOUT_CALLBACK).version(V_1_0_A).build();
    private static final ServiceProviderToken AUTHORIZED_REQUEST_TOKEN = UNAUTHORIZED_REQUEST_TOKEN.authorize(USER, "9876");
    private static final ServiceProviderToken AUTHORIZED_REQUEST_TOKEN_WITH_CUSTOM_CALLBACK = UNAUTHORIZED_REQUEST_TOKEN_WITH_CUSTOM_CALLBACK.authorize(USER, "9876");
    private static final ServiceProviderToken AUTHORIZED_REQUEST_TOKEN_WITHOUT_CALLBACK = UNAUTHORIZED_REQUEST_TOKEN_WITHOUT_CALLBACK.authorize(USER, "9876");
    private static final ServiceProviderToken DENIED_REQUEST_TOKEN = UNAUTHORIZED_REQUEST_TOKEN.deny(USER);
    private static final ServiceProviderToken DENIED_REQUEST_TOKEN_WITHOUT_CALLBACK = UNAUTHORIZED_REQUEST_TOKEN_WITHOUT_CALLBACK.deny(USER);

    private static final ServiceProviderToken UNAUTHORIZED_REQUEST_TOKEN_V1 = ServiceProviderToken.newRequestToken("1234").tokenSecret("5678").consumer(RSA_CONSUMER).version(V_1_0).build();
    private static final ServiceProviderToken AUTHORIZED_REQUEST_TOKEN_V1 = UNAUTHORIZED_REQUEST_TOKEN_V1.authorize(USER, "9876");
    private static final ServiceProviderToken DENIED_REQUEST_TOKEN_V1 = UNAUTHORIZED_REQUEST_TOKEN_V1.deny(USER);
    private static final ServiceProviderToken UNAUTHORIZED_REQUEST_TOKEN_WITHOUT_CALLBACK_V1 = ServiceProviderToken.newRequestToken("1234").tokenSecret("5678").consumer(CONSUMER_WITHOUT_CALLBACK).version(V_1_0).build();
    private static final ServiceProviderToken AUTHORIZED_REQUEST_TOKEN_WITHOUT_CALLBACK_V1 = UNAUTHORIZED_REQUEST_TOKEN_WITHOUT_CALLBACK_V1.authorize(USER, "9876");

    @Mock
    ServiceProviderTokenStore store;
    @Mock
    Randomizer randomizer;
    @Mock
    UserManager userManager;
    @Mock
    AuthorizationRenderer renderer;
    @Mock
    TemplateRenderer templateRenderer;

    AuthorizationRequestProcessor post;

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;

    ByteArrayOutputStream responseStream;

    @Before
    public void setUp() throws Exception {
        responseStream = new ByteArrayOutputStream();
        when(response.getOutputStream()).thenReturn(new ByteArrayServletOutputStream(responseStream));
        when(response.getWriter()).thenReturn(new PrintWriter(new OutputStreamWriter(responseStream)));

        post = new PostAuthorization(store, randomizer, userManager, renderer, templateRenderer);
    }

    @Test
    public void verifyThatPostWithApproveParameterSavesAuthorizedRequestTokenAndRedirectsUserToConsumerCallback() throws Exception {
        when(request.getParameter("approve")).thenReturn("Value doesn't matter");
        when(randomizer.randomAlphanumericString(anyInt())).thenReturn("9876");
        when(userManager.getRemoteUsername(request)).thenReturn("bob");
        when(userManager.resolve("bob")).thenReturn(USER);
        when(store.put((ServiceProviderToken) argThat(is(equalTo(AUTHORIZED_REQUEST_TOKEN))))).thenReturn(AUTHORIZED_REQUEST_TOKEN);

        post.process(request, response, UNAUTHORIZED_REQUEST_TOKEN);

        verify(store).put((ServiceProviderToken) argThat(is(equalTo(AUTHORIZED_REQUEST_TOKEN))));
        verify(response).sendRedirect((String) argThat(allOf(
                Matchers.startsWith("http://consumer/callback?"),
                containsString("oauth_token=1234"),
                containsString("oauth_verifier=9876")
        )));
    }

    @Test
    public void verifyThatApprovingWithCallbackParameterIsIgnoredWhenApprovingTokens() throws Exception {
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
                OAUTH_CALLBACK, new String[]{"http://evil/callback"}
        ));
        when(request.getParameter("approve")).thenReturn("Value doesn't matter");
        when(request.getParameter(OAUTH_CALLBACK)).thenReturn("http://evil/callback");
        when(randomizer.randomAlphanumericString(anyInt())).thenReturn("9876");
        when(store.put((ServiceProviderToken) argThat(is(equalTo(AUTHORIZED_REQUEST_TOKEN))))).thenReturn(AUTHORIZED_REQUEST_TOKEN);
        when(userManager.getRemoteUsername(request)).thenReturn("bob");
        when(userManager.resolve("bob")).thenReturn(USER);

        post.process(request, response, UNAUTHORIZED_REQUEST_TOKEN);

        verify(store).put((ServiceProviderToken) argThat(is(equalTo(AUTHORIZED_REQUEST_TOKEN))));
        verify(response).sendRedirect((String) argThat(allOf(
                Matchers.startsWith("http://consumer/callback?"),
                containsString("oauth_token=1234"),
                containsString("oauth_verifier=9876")
        )));
    }

    @Test
    public void verifyThatApprovalWillRedirectUserToConsumerCallbackFromRequestTokenIfItHasOne() throws Exception {
        when(request.getParameter("approve")).thenReturn("Value doesn't matter");
        when(randomizer.randomAlphanumericString(anyInt())).thenReturn("9876");
        when(store.put((ServiceProviderToken) argThat(is(equalTo(AUTHORIZED_REQUEST_TOKEN_WITH_CUSTOM_CALLBACK))))).thenReturn(AUTHORIZED_REQUEST_TOKEN_WITH_CUSTOM_CALLBACK);
        when(userManager.getRemoteUsername(request)).thenReturn("bob");
        when(userManager.resolve("bob")).thenReturn(USER);

        post.process(request, response, UNAUTHORIZED_REQUEST_TOKEN_WITH_CUSTOM_CALLBACK);

        verify(store).put((ServiceProviderToken) argThat(is(equalTo(AUTHORIZED_REQUEST_TOKEN_WITH_CUSTOM_CALLBACK))));
        verify(response).sendRedirect((String) argThat(allOf(
                Matchers.startsWith("http://consumer/other-callback?"),
                containsString("oauth_token=1234"),
                containsString("oauth_verifier=9876")
        )));
    }

    @Test
    public void verifyThatApprovingTokenWithNoCallbackDisplaysVerifier() throws Exception {
        when(request.getParameter("approve")).thenReturn("Value doesn't matter");
        when(randomizer.randomAlphanumericString(anyInt())).thenReturn("9876");
        when(store.get("1234")).thenReturn(UNAUTHORIZED_REQUEST_TOKEN_WITHOUT_CALLBACK);
        when(store.put((ServiceProviderToken) argThat(is(equalTo(AUTHORIZED_REQUEST_TOKEN_WITHOUT_CALLBACK))))).thenReturn(AUTHORIZED_REQUEST_TOKEN_WITHOUT_CALLBACK);
        when(userManager.getRemoteUsername(request)).thenReturn("bob");
        when(userManager.resolve("bob")).thenReturn(USER);

        post.process(request, response, UNAUTHORIZED_REQUEST_TOKEN_WITHOUT_CALLBACK);

        verify(store).put((ServiceProviderToken) argThat(is(equalTo(AUTHORIZED_REQUEST_TOKEN_WITHOUT_CALLBACK))));
        verify(templateRenderer).render(eq("templates/auth/no-callback-approval-v1a.vm"), anyMap(), isA(Writer.class));
    }

    @Test
    public void verifyThatDenyingRedirectsUserToConsumerCallback() throws Exception {
        when(request.getParameter("deny")).thenReturn("Value doesn't matter");
        when(store.get("1234")).thenReturn(UNAUTHORIZED_REQUEST_TOKEN);
        when(store.put((ServiceProviderToken) argThat(is(equalTo(DENIED_REQUEST_TOKEN))))).thenReturn(DENIED_REQUEST_TOKEN);
        when(userManager.getRemoteUsername(request)).thenReturn("bob");
        when(userManager.resolve("bob")).thenReturn(USER);

        post.process(request, response, UNAUTHORIZED_REQUEST_TOKEN);

        verify(store).put((ServiceProviderToken) argThat(is(equalTo(DENIED_REQUEST_TOKEN))));
        verify(response).sendRedirect((String) argThat(allOf(
                Matchers.startsWith("http://consumer/callback?"),
                containsString("oauth_token=1234"),
                containsString("oauth_verifier=denied")
        )));
    }

    @Test
    public void verifyThatDenyingTokenWithNoCallbackDisplaysDenialMessage() throws Exception {
        when(request.getParameter("deny")).thenReturn("Value doesn't matter");
        when(randomizer.randomAlphanumericString(anyInt())).thenReturn("9876");
        when(store.get("1234")).thenReturn(UNAUTHORIZED_REQUEST_TOKEN_WITHOUT_CALLBACK);
        when(store.put((ServiceProviderToken) argThat(is(equalTo(DENIED_REQUEST_TOKEN_WITHOUT_CALLBACK))))).thenReturn(DENIED_REQUEST_TOKEN_WITHOUT_CALLBACK);
        when(userManager.getRemoteUsername(request)).thenReturn("bob");
        when(userManager.resolve("bob")).thenReturn(USER);

        post.process(request, response, UNAUTHORIZED_REQUEST_TOKEN_WITHOUT_CALLBACK);

        verify(store).put((ServiceProviderToken) argThat(is(equalTo(DENIED_REQUEST_TOKEN_WITHOUT_CALLBACK))));
        verify(templateRenderer).render(eq("templates/auth/no-callback-denied.vm"), anyMap(), isA(PrintWriter.class));
    }

    @Test
    public void verifyThatCallbackParameterIsIgnoredWhenDenyingTokens() throws Exception {
        when(request.getParameterMap()).thenReturn(ImmutableMap.of(
                OAUTH_CALLBACK, new String[]{"http://evil/callback"}
        ));
        when(request.getParameter("deny")).thenReturn("Value doesn't matter");
        when(randomizer.randomAlphanumericString(anyInt())).thenReturn("9876");
        when(store.get("1234")).thenReturn(UNAUTHORIZED_REQUEST_TOKEN);
        when(store.put((ServiceProviderToken) argThat(is(equalTo(DENIED_REQUEST_TOKEN))))).thenReturn(DENIED_REQUEST_TOKEN);
        when(userManager.getRemoteUsername(request)).thenReturn("bob");
        when(userManager.resolve("bob")).thenReturn(USER);

        post.process(request, response, UNAUTHORIZED_REQUEST_TOKEN);

        verify(store).put((ServiceProviderToken) argThat(is(equalTo(DENIED_REQUEST_TOKEN))));
        verify(response).sendRedirect((String) argThat(allOf(
                Matchers.startsWith("http://consumer/callback?"),
                containsString("oauth_token=1234"),
                containsString("oauth_verifier=denied")
        )));
    }

    @Ignore
    @Test
    public void verifyThatCallbackParameterIsUsedAfterApprovalIfRequestTokenIsAVersion1Token() throws Exception {
        when(request.getParameter("approve")).thenReturn("Value doesn't matter");
        when(request.getParameter(OAUTH_CALLBACK)).thenReturn("http://callback/from/parameter");
        when(randomizer.randomAlphanumericString(anyInt())).thenReturn("9876");
        when(userManager.getRemoteUsername(request)).thenReturn("bob");
        when(userManager.resolve("bob")).thenReturn(USER);
        when(store.put((ServiceProviderToken) argThat(is(equalTo(AUTHORIZED_REQUEST_TOKEN_V1))))).thenReturn(AUTHORIZED_REQUEST_TOKEN_V1);

        post.process(request, response, UNAUTHORIZED_REQUEST_TOKEN_V1);

        verify(store).put((ServiceProviderToken) argThat(is(equalTo(AUTHORIZED_REQUEST_TOKEN_V1))));
        verify(response).sendRedirect((String) argThat(allOf(
                Matchers.startsWith("http://callback/from/parameter?"),
                containsString("oauth_token=1234"),
                not(containsString("oauth_verifier="))
        )));
    }

    @Test
    public void verifyThatCallbackParameterIsUsedAfterDenialIfRequestTokenIsAVersion1Token() throws Exception {
        when(request.getParameter("deny")).thenReturn("Value doesn't matter");
        when(request.getParameter(OAUTH_CALLBACK)).thenReturn("http://callback/from/parameter");
        when(randomizer.randomAlphanumericString(anyInt())).thenReturn("9876");
        when(userManager.getRemoteUsername(request)).thenReturn("bob");
        when(userManager.resolve("bob")).thenReturn(USER);
        when(store.put((ServiceProviderToken) argThat(is(equalTo(DENIED_REQUEST_TOKEN_V1))))).thenReturn(DENIED_REQUEST_TOKEN_V1);

        post.process(request, response, UNAUTHORIZED_REQUEST_TOKEN_V1);

        verify(store).put((ServiceProviderToken) argThat(is(equalTo(DENIED_REQUEST_TOKEN_V1))));
        verify(response).sendRedirect((String) argThat(allOf(
                Matchers.startsWith("http://callback/from/parameter?"),
                containsString("oauth_token=1234"),
                not(containsString("oauth_verifier="))
        )));
    }

    @Test
    public void verifyThatVersion1ApprovalMessageIsDisplayedIfNoCallbackIsProviderInEitherConsumerOrTheRequest() throws Exception {
        when(request.getParameter("approve")).thenReturn("Value doesn't matter");
        when(randomizer.randomAlphanumericString(anyInt())).thenReturn("9876");
        when(store.get("1234")).thenReturn(UNAUTHORIZED_REQUEST_TOKEN_WITHOUT_CALLBACK_V1);
        when(store.put((ServiceProviderToken) argThat(is(equalTo(AUTHORIZED_REQUEST_TOKEN_WITHOUT_CALLBACK_V1))))).thenReturn(AUTHORIZED_REQUEST_TOKEN_WITHOUT_CALLBACK_V1);
        when(userManager.getRemoteUsername(request)).thenReturn("bob");
        when(userManager.resolve("bob")).thenReturn(USER);

        post.process(request, response, UNAUTHORIZED_REQUEST_TOKEN_WITHOUT_CALLBACK_V1);

        verify(store).put((ServiceProviderToken) argThat(is(equalTo(AUTHORIZED_REQUEST_TOKEN_WITHOUT_CALLBACK_V1))));
        verify(templateRenderer).render(eq("templates/auth/no-callback-approval-v1.vm"), anyMap(), isA(Writer.class));
    }

    @Test
    public void verifyThatUserIsRedirectedToConsumerCallbackIfCallbackParameterIsMissingAndTokenIsAVersion1Token() throws Exception {
        when(request.getParameter("approve")).thenReturn("Value doesn't matter");
        when(randomizer.randomAlphanumericString(anyInt())).thenReturn("9876");
        when(userManager.getRemoteUsername(request)).thenReturn("bob");
        when(userManager.resolve("bob")).thenReturn(USER);
        when(store.put((ServiceProviderToken) argThat(is(equalTo(AUTHORIZED_REQUEST_TOKEN_V1))))).thenReturn(AUTHORIZED_REQUEST_TOKEN_V1);

        post.process(request, response, UNAUTHORIZED_REQUEST_TOKEN_V1);

        verify(store).put((ServiceProviderToken) argThat(is(equalTo(AUTHORIZED_REQUEST_TOKEN_V1))));
        verify(response).sendRedirect((String) argThat(allOf(
                Matchers.startsWith("http://consumer/callback?"),
                containsString("oauth_token=1234"),
                not(containsString("oauth_verifier=9876"))
        )));
    }
}
