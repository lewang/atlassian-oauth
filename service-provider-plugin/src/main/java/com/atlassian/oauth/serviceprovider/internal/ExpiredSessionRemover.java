package com.atlassian.oauth.serviceprovider.internal;

import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.scheduler.compat.JobHandler;
import com.atlassian.scheduler.compat.JobInfo;

/**
 * Calls the {@link ServiceProviderTokenStore#removeExpiredTokensAndNotify()} and {@link
 * ServiceProviderTokenStore#removeExpiredSessionsAndNotify()} method to remove expired tokens without session
 * information (legacy tokens) and tokens whose sessions have expired.
 */
public class ExpiredSessionRemover implements JobHandler {

    private final ServiceProviderTokenStore tokenStore;

    public ExpiredSessionRemover(ServiceProviderTokenStore tokenStore) {
        this.tokenStore = tokenStore;
    }

    @Override
    public void execute(JobInfo jobInfo) {
        tokenStore.removeExpiredTokensAndNotify();
        tokenStore.removeExpiredSessionsAndNotify();
    }
}
