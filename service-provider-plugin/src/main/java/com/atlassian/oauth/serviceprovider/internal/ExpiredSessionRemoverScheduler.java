package com.atlassian.oauth.serviceprovider.internal;

import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.scheduler.compat.CompatibilityPluginScheduler;
import com.atlassian.scheduler.compat.JobHandlerKey;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Date;

/**
 * Schedules the {@link ExpiredSessionRemover} to run every 8 hours.
 */
public final class ExpiredSessionRemoverScheduler implements LifecycleAware {
    private static final long EIGHT_HOURS = 28800000;
    private static final String HANDLER_KEY = "Service Provider Session Remover";
    private final ServiceProviderTokenStore store;
    private final CompatibilityPluginScheduler pluginScheduler;

    public ExpiredSessionRemoverScheduler(@Qualifier("tokenStore") ServiceProviderTokenStore store,
                                          CompatibilityPluginScheduler pluginScheduler) {
        this.store = store;
        this.pluginScheduler = pluginScheduler;
    }

    @Override
    public void onStart() {
        JobHandlerKey handlerKey = JobHandlerKey.of(HANDLER_KEY);
        pluginScheduler.registerJobHandler(handlerKey,
                new ExpiredSessionRemover(store));
        pluginScheduler.scheduleClusteredJob(handlerKey.toString(), handlerKey,
                new Date(System.currentTimeMillis() + EIGHT_HOURS),
                EIGHT_HOURS);
    }

    @Override
    public void onStop() {
        JobHandlerKey handlerKey = JobHandlerKey.of(HANDLER_KEY);
        pluginScheduler.unregisterJobHandler(handlerKey);
        pluginScheduler.unscheduleClusteredJob(handlerKey.toString());
    }
}
