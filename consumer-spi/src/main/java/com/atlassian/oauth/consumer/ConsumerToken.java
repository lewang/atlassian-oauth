package com.atlassian.oauth.consumer;

import com.atlassian.oauth.Token;
import net.jcip.annotations.Immutable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Representation of an OAuth token for use by consumers.  A token can be either a request token or an access token.
 * Tokens always have a token value, a token secret and the {@code Consumer} the token belongs to.  Tokens also have
 * a set of option properties to store additional information, such as properties that might be required by extensions
 * to the OAuth spec.
 *
 * <p>Token instances are immutable.  To create new {@code ConsumerToken} instances, use the builder.  To build a
 * request token, use
 * <pre>
 *   ConsumerToken requestToken = ConsumerToken.newRequestToken("bb6dd1391ce33b5bd3ecad1175139a39")
 *          .tokenSecret("29c3005cc5fbe5d431f27b29d6191ea3")
 *          .consumer(consumer)
 *          .build();
 * </pre>
 *
 * <p>To build an access token, use the {@link #newAccessToken(String)} method as the starting point
 * <pre>
 *   ConsumerToken accessToken = ConsumerToken.newAccessToken("bb6dd1391ce33b5bd3ecad1175139a39")
 *          .tokenSecret("29c3005cc5fbe5d431f27b29d6191ea3")
 *          .consumer(consumer)
 *          .build();
 * </pre>
 */
@Immutable
public final class ConsumerToken extends Token {
    private ConsumerToken(ConsumerTokenBuilder builder) {
        super(builder);
    }

    /**
     * Static factory method that starts the process of building a request {@code ConsumerToken} instance.  Returns a
     * {@code ConsumerTokenBuilder} so the additional token attributes can be set.
     *
     * @param token unique token used to the {@code ConsumerToken} to be used in OAuth operations
     * @return the builder to set additional token attributes and build the {@code ConsumerToken}
     */
    public static ConsumerTokenBuilder newRequestToken(String token) {
        return new ConsumerTokenBuilder(Type.REQUEST, checkNotNull(token, "token"));
    }

    /**
     * Static factory method that starts the process of building an access {@code ConsumerToken} instance.  Returns a
     * {@code ConsumerTokenBuilder} so the additional token attributes can be set.
     *
     * @param token unique token used to the {@code ConsumerToken} to be used in OAuth operations
     * @return the builder to set additional token attributes and build the {@code ConsumerToken}
     */
    public static ConsumerTokenBuilder newAccessToken(String token) {
        return new ConsumerTokenBuilder(Type.ACCESS, checkNotNull(token, "token"));
    }

    /**
     * Builder for constructing {@code ConsumerToken}s.
     */
    public static final class ConsumerTokenBuilder extends TokenBuilder<ConsumerToken, ConsumerTokenBuilder> {
        public ConsumerTokenBuilder(Type type, String token) {
            super(type, token);
        }

        /**
         * Constructs and returns the final request {@code ConsumerToken} instance.
         *
         * @return the final request {@code ConsumerToken} instance
         */
        public ConsumerToken build() {
            return new ConsumerToken(this);
        }
    }
}
