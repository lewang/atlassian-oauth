package com.atlassian.oauth.consumer;

/**
 * Thrown when there is a problem creating a {@link com.atlassian.oauth.Consumer} instance.  This is usually because the RSA algorithm
 * is not provided by any installed encryption providers, the stored public key is not valid, or the stored callback
 * URI is invalid.
 */
public class ConsumerCreationException extends RuntimeException {
    public ConsumerCreationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConsumerCreationException(String message) {
        super(message);
    }
}
