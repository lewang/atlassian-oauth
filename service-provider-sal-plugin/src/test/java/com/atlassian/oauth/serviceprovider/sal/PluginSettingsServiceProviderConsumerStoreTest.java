package com.atlassian.oauth.serviceprovider.sal;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore;
import com.atlassian.oauth.util.RSAKeys;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.hamcrest.Matcher;
import org.hamcrest.core.CombinableMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static com.atlassian.oauth.testing.Matchers.equalTo;
import static com.atlassian.oauth.testing.Matchers.mapWithKeys;
import static com.atlassian.oauth.testing.Matchers.withStringLength;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER;
import static com.atlassian.oauth.testing.TestData.Consumers.RSA_CONSUMER_WITH_LONG_KEY;
import static com.atlassian.oauth.testing.TestData.KEYS;
import static org.hamcrest.CoreMatchers.both;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PluginSettingsServiceProviderConsumerStoreTest {
    @Mock
    PluginSettingsFactory pluginSettingsFactory;

    ServiceProviderConsumerStore store;

    Map<String, Object> settings;

    @Before
    public void setUp() {
        settings = new HashMap<String, Object>();
        when(pluginSettingsFactory.createGlobalSettings()).thenReturn(new MapBackedPluginSettings(settings));

        store = new PluginSettingsServiceProviderConsumerStore(pluginSettingsFactory);
    }

    @Test
    public void assertThatConsumersAreStoredAsProperties() {
        store.put(RSA_CONSUMER);

        assertThat(
                (Properties) settings.get(ServiceProviderConsumerStore.class.getName() + ".consumer." + RSA_CONSUMER.getKey()),
                is(equalTo(asProperties(RSA_CONSUMER)))
        );
        assertThat((String) settings.get(ServiceProviderConsumerStore.class.getName() + ".allConsumerKeys"), is(equalTo(RSA_CONSUMER.getKey())));
    }

    @Test
    public void assertThatRemoveSetsPluginSettingsValuesToNull() {
        settings.put(ServiceProviderConsumerStore.class.getName() + ".consumer." + RSA_CONSUMER.getKey(), asProperties(RSA_CONSUMER));
        settings.put(ServiceProviderConsumerStore.class.getName() + ".allConsumerKeys", "consumer-rsa");

        store.remove(RSA_CONSUMER.getKey());

        assertNull(settings.get(ServiceProviderConsumerStore.class.getName() + ".consumer." + RSA_CONSUMER.getKey()));
        assertNull("consumerKeys", settings.get(ServiceProviderConsumerStore.class.getName() + ".allConsumerKeys"));
    }

    @Test
    public void assertThatConsumersAreRetrievedFromProperties() {
        settings.put(ServiceProviderConsumerStore.class.getName() + ".consumer." + RSA_CONSUMER.getKey(), asProperties(RSA_CONSUMER));

        assertThat(store.get(RSA_CONSUMER.getKey()), is(equalTo(RSA_CONSUMER)));
    }

    @Test
    public void assertThatAllConsumersAreRetrieved() {
        Consumer other = Consumer.key("other").callback(URI.create("http://other/callback")).description("other description").name("other consumer").publicKey(KEYS.getPublic()).build();

        settings.put(ServiceProviderConsumerStore.class.getName() + ".allConsumerKeys", "consumer-rsa/other");
        settings.put(ServiceProviderConsumerStore.class.getName() + ".consumer." + RSA_CONSUMER.getKey(), asProperties(RSA_CONSUMER));
        settings.put(ServiceProviderConsumerStore.class.getName() + ".consumer." + other.getKey(), asProperties(other));

        // i'd like to put this all in the assertThat method so it would be both(hasConsumer(RSA_CONSUMER)).and(hasConsumer(other))
        // but the Sun java compiler is flaky (it works in Eclipse though)
        CombinableMatcher.CombinableBothMatcher<Iterable<? super Consumer>> hasRsaConsumer = both(hasConsumer(RSA_CONSUMER));
        assertThat(store.getAll(), hasRsaConsumer.and(hasConsumer(other)));
    }

    @Test
    public void assertThatWhatIsStoredIsActuallyReturnedAsIs() {
        Consumer consumer1 = Consumer.key("consumer1").callback(URI.create("http://other/callback")).description("other description").name("other consumer")
                .threeLOAllowed(false).twoLOAllowed(true).executingTwoLOUser("user1").twoLOImpersonationAllowed(true).publicKey(KEYS.getPublic()).build();

        store.put(consumer1);

        assertThat(store.get("consumer1"), samePropertyValuesAs(consumer1));
    }

    @Test
    public void assertThatOptional2LOParamatersAreAssumedAsFalseByDefault() {
        Consumer consumer1 = Consumer.key("consumer1").callback(URI.create("http://other/callback")).description("other description").name("other consumer").publicKey(KEYS.getPublic()).build();

        store.put(consumer1);

        Consumer retrievedConsumer1 = store.get("consumer1");
        assertThat(retrievedConsumer1.getTwoLOAllowed(), is(false));
        assertThat(retrievedConsumer1.getTwoLOImpersonationAllowed(), is(false));
    }

    @Test
    public void assertThatPre2LOConsumersAreTreatedAs2LODisabled() {
        Properties props = new Properties();
        props.put("name", "name1");
        props.put("description", "description1");
        props.put("callback", "callback1");
        props.put("publicKey", RSAKeys.toPemEncoding(RSA_CONSUMER.getPublicKey()));

        settings.put(ServiceProviderConsumerStore.class.getName() + ".consumer." + RSA_CONSUMER.getKey(), props);
        settings.put(ServiceProviderConsumerStore.class.getName() + ".allConsumerKeys", "consumer-rsa");

        Consumer consumer = store.get(RSA_CONSUMER.getKey());

        assertThat(consumer.getTwoLOAllowed(), is(false));
        assertNull(consumer.getExecutingTwoLOUser());
        assertThat(consumer.getTwoLOImpersonationAllowed(), is(false));
    }

    @Test
    public void assertThat3LOFlagMissingMeansItsEnabled() {
        Properties props = new Properties();
        props.put("name", "name1");
        props.put("description", "description1");
        props.put("callback", "callback1");
        props.put("publicKey", RSAKeys.toPemEncoding(RSA_CONSUMER.getPublicKey()));

        settings.put(ServiceProviderConsumerStore.class.getName() + ".consumer." + RSA_CONSUMER.getKey(), props);
        settings.put(ServiceProviderConsumerStore.class.getName() + ".allConsumerKeys", "consumer-rsa");

        Consumer consumer = store.get(RSA_CONSUMER.getKey());
        assertThat(consumer.getThreeLOAllowed(), is(true));
    }

    @Test
    public void assertThatConsumerPropertyKeysAreLessThanOneHundredCharacters() {
        store.put(RSA_CONSUMER_WITH_LONG_KEY);

        assertThat(settings, mapWithKeys(withStringLength(lessThanOrEqualTo(100))));
    }

    private Matcher<Iterable<? super Consumer>> hasConsumer(Consumer consumer) {
        return hasItem(equalTo(consumer));
    }

    Properties asProperties(Consumer consumer) {
        Properties properties = new Properties();
        properties.put("name", consumer.getName());
        properties.put("description", consumer.getDescription());
        properties.put("callback", consumer.getCallback().toString());
        properties.put("publicKey", RSAKeys.toPemEncoding(consumer.getPublicKey()));
        properties.put("threeLOAllowed", Boolean.toString(consumer.getThreeLOAllowed()));
        properties.put("twoLOAllowed", Boolean.toString(consumer.getTwoLOAllowed()));
        if (consumer.getExecutingTwoLOUser() != null) {
            properties.put("executingTwoLOUser", consumer.getExecutingTwoLOUser());
        }
        properties.put("twoLOImpersonationAllowed", Boolean.toString(consumer.getTwoLOImpersonationAllowed()));
        return properties;
    }
}
