package com.atlassian.oauth.shared.sal;

import com.google.common.collect.Maps;

import java.util.Map;
import java.util.Properties;

public abstract class AbstractSettingsProperties {
    private final Map<String, String> properties;

    public AbstractSettingsProperties() {
        properties = Maps.newHashMap();
    }

    public AbstractSettingsProperties(Properties properties) {
        this.properties = Maps.fromProperties(properties);
    }

    public final Properties asProperties() {
        Properties props = new Properties();
        for (Map.Entry<String, String> prop : properties.entrySet()) {
            if (prop.getValue() != null) {
                props.put(prop.getKey(), prop.getValue());
            }
        }
        return props;
    }

    protected final void put(String key, String value) {
        properties.put(key, value);
    }

    protected final String get(String key) {
        return properties.get(key);
    }
}
