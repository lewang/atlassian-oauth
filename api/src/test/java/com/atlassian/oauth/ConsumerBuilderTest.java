package com.atlassian.oauth;

import org.junit.Before;
import org.junit.Test;

import java.net.URI;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ConsumerBuilderTest {
    private Consumer defaultConsumer;
    private URI uri;

    @Before
    public void setUp() {
        URI uri = URI.create("http://www.atlassian.com");
        defaultConsumer = Consumer.key("key").name("name")
                .description("description").callback(uri)
                .signatureMethod(Consumer.SignatureMethod.HMAC_SHA1)
                .build();
    }

    @Test
    public void assertByDefaultConsumerBuilderCreates3LOEnabledConsumer() {
        assertTrue(defaultConsumer.getThreeLOAllowed());
    }

    @Test
    public void assertByDefaultConsumerBuilderCreates2LODisabledConsumer() {
        assertFalse(defaultConsumer.getTwoLOAllowed());
    }

    @Test
    public void assertByDefaultConsumerBuilderCreates2LOWithImpersonationDisabledConsumer() {
        assertFalse(defaultConsumer.getTwoLOImpersonationAllowed());
    }

    @Test
    public void assertConsumerCanBeCreatedWith3LOOff() {
        Consumer consumer = Consumer.key("key").name("name")
                .description("description").callback(uri)
                .signatureMethod(Consumer.SignatureMethod.HMAC_SHA1)
                .threeLOAllowed(false)
                .build();
        assertFalse(consumer.getThreeLOAllowed());
    }
}
