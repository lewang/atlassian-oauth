package com.atlassian.oauth.event;

/**
 * This event is published when a request token is removed from the service provider.
 */
public class RequestTokenRemovedEvent extends TokenRemovedEvent {
    public RequestTokenRemovedEvent(String username) {
        super(username);
    }
}
