package com.atlassian.oauth.event;

/**
 * This event is published when an access token is removed from the service provider.
 */
public class AccessTokenRemovedEvent extends TokenRemovedEvent {
    public AccessTokenRemovedEvent(String username) {
        super(username);
    }
}
